import React from 'react';
import {View, Text} from 'react-native';
import { Button } from '../../components';
import { colors } from '../../utils';




    const ActionButton = ({desc, title, onPress}) => {

        return (
        <View style={styles.wrapper.component}>
            <View>
            <Text style={styles.wrapper.desc}>
                {desc}
                </Text>
                <Button title={title} onPress={onPress} />
            
            
            </View>
        </View>
        )
        
    }

    const styles = {
        wrapper: {
            component:
            {
                marginBottom:43, 
                maxWidth:225
            },
            
            desc:
            {
                fontSize:10, 
                color:colors.text.default, 
                textAlign:'center', 
                marginHorizontal:15, 
                marginBottom:6,
            }

            
        }
    }

    export default ActionButton;