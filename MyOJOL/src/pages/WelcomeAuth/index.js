import React from 'react';
import {View, Text, Image} from 'react-native';
import {a} from '../../assets';
import { colors } from '../../utils';
import ActionButton from './ActionButton';

    const WelcomeAuth = ({navigation}) => {
        const handleGoto = (screen) => {
            navigation.navigate(screen);
        };
     return(
         <View style={styles.wrapper.page}>
          
          <Image source={a} style={styles.wrapper.illustration}/>
            <Text style={styles.text.welcome}>
                    Selamat Datang Di App Jek-OL</Text>

            <ActionButton 
            desc="Silahkan Login, Jika anda sudah memiliki akun" 
            title="LOGIN"
            onPress={() => handleGoto('Login')}
            />

            <ActionButton 
            desc="atau silahkan register, jika anda belum memiliki akun" 
            title="REGISTER"
            onPress={() => handleGoto('Register')}
            />
            
        </View>
       
    );
}

const styles= {


    wrapper: {
        page:{

            alignItems:'center', 
            justifyContent:'center',
            flex:1,
            backgroundColor:'white',

        },
    
    illustration:
        {
            width: 219, 
            height: 186, 
            marginBottom:10,
        }
    },

text: {
    welcome: 
        {
            fontSize: 18, 
            fontWeight:'bold', 
            color:'#12CBC4', 
            marginBottom:76,
        },
    }


}

export default WelcomeAuth;