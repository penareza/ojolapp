import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Splash, Login, Register, WelcomeAuth } from '../pages';


// call function stack
const Stack = createStackNavigator();


// create component router, adalah kumpulan navigasi-navigasi

const Router = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Splash" component={Splash} />
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={Register }/>
            <Stack.Screen 
            name="WelcomeAuth" 
            component={WelcomeAuth}  
            options={{
                headerShown: false,
            }}/>
        </Stack.Navigator>
    )
}

export default Router;


